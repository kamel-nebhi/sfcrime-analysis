# SF Crime data analysis

In this project, I will analyze the San Francisco crime dataset available at https://www.kaggle.com/c/sf-crime/data. 

First, I will describe the dataset and the data preparation process. Then, I will present my data analysis process using Hadoop and Hive technologies and I will have a general analysis about these data. 
Next, I will enrich these data using NLP processing in order to extract interesting information about DRUG/NARCOTICS crime category.

Slides about this work: [here](http://cms.unige.ch/lettres/linguistique/nebhi/_data/report.pdf)
## Understand and prepare the data

### Answering questions about data…	

* What is the general trend of crime? Per category, district,...	
* What are the Top 10 crimes reported in San Francisco?	
* What is the safest day and?
* ...

* Analyze of Drug and Narco crimes behaviour:
	* Fight and	prevent	drugs and narcotics problems		
	* Security service and Health service can exploit these analysis
	* Analyze
		* drug category according to the U.S. schedule (I,II,III,IV,V)
		* drug classification (stimulant, depressant, opium, etc.)
		* crime category (possession, transportation, planting, sale, etc.)	
	* Use the unstructured information present in the _description_ field
	* Which drug categories are mostly used by district?
	* What is the trend of drug category, drug classification and drug crime category?
	* ...

### Data description

This dataset is provided by [SF OpenData](https://data.sfgov.org/), the central clearinghouse for data published by the City and County of San Francisco.
It contains incidents derived from SFPD Crime Incident Reporting system ranges from 1/1/2003 to 5/13/2015. The training set and test set rotate every week, meaning week 1,3,5,7... belong to test set, week 2,4,6,8 belong to training set. 

Tre training set contains 878050 entries and the test set contains 884263 entries.

The data fields for these files are:
1. **Dates** - timestamp of the crime incident
2. **Category** - category of the crime incident (_only in train.csv_).
3. **Descript** - detailed description of the crime incident (_only in train.csv_)
4. **DayOfWeek** - the day of the week
5. **PdDistrict** - name of the Police Department District
6. **Resolution** - how the crime incident was resolved (_only in train.csv_)
7. **Address** - the approximate street address of the crime incident 
8. **X** - Longitude
9. **Y** - Latitude

### Data preparation

For this experiment, we will use the training set as it contains _category_, _descript_ and _resolution_ fields.

First, we will transform data into a form so that it is appropriate for our data analysis job.

We have to parse the _DayOfWeek_ field in order to obtain two separate fields (_date_ and _hour_). Then, we'll have to
exclude noisy data.


```python
__author__ = 'kamelnebhi'

import pandas as pd
from sklearn import preprocessing
import numpy as np

#read csv file
train = pd.read_csv("../input/train.csv", parse_dates=[0], infer_datetime_format=True, error_bad_lines=False)

#split datetime to date and time columns
temp = pd.DatetimeIndex(train['Dates'])
train['Date'] = temp.date
train['Time'] = temp.time
#delete Dates column
del train['Dates']

#create output
cols_to_keep = ['Date', 'Time','DayOfWeek','Category','PdDistrict','Resolution','Descript','Address','X','Y']
output = train[cols_to_keep]
output.to_csv("train-norm.csv", sep='\t', encoding='utf-8')

#print output.head()

#summarize the data
print output.describe()
```

### Data enrichment

The goal is to enrich data by using IE technologies. We want to transform unstructured 
into structured data.

We'll exploit the _descript_ field in order to extract interesting information.

Our approach is built on GATE and is similar to NER for drug and narcotic.

More details about this plugin here: https://gitlab.com/kamel-nebhi/nerdrugnarcotic

## Data analysis

Our strategy to analyze Big Data is based on Hadoop. We also choose Hive to query the dataset.

We now explain how to analyze the SFPD dataset using Hadoop and Hive technologies.

First, we have to import our CSV file into HDFS:

```sh
bin/hadoop dfs -put /Users/data/sfpdcrime/train-norm.csv /user/hive/warehouse/sfpdcrime
```

Alternatively, we can load data into table using a local file:

```sql
LOAD DATA LOCAL INPATH '/Users/data/sfpdcrime/train-norm.csv' OVERWRITE INTO TABLE sfpdcrime;
```

```sql
LOAD DATA LOCAL INPATH '/Users/data/sfpd-crimes/sfpd-drugnarcotic‘ OVERWRITE INTO TABLE sfpddrug;
```

Then, we create a HIVE table for data loading and querying:

```sql
CREATE TABLE sfpdcrime(
IncidentNum string,
CrimeDate string,
CrimeTime string,
DayOfWeek string,
Category string,
PdDistrict string,
Resolution string,
Descript string,
Address string,
X string,
Y string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t';
```

We also create a table related to the IE job:
```sql
CREATE TABLE sfpddrug(Descript string, crimecategory string, drugcategory string, drugclassification string) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
```

Finally, we can now query the table and write the results in a directory like that:

```sql
INSERT OVERWRITE LOCAL DIRECTORY '/Users/data/hive-output' ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
```


### General analysis

To analyze the evolution of crime from January 2003 to May 2015, we query the table like that:

```sql
 SELECT concat(year(crimedate),month(crimedate)), COUNT(*) FROM sfpdcrime2 GROUP BY concat(year(crimedate),month(crimedate));
```

<img src="img/Evolutionofcrimefrom2003to2014.png" width="60%">


To analyze the distribution of crime by category, we query the table like that:

```sql
SELECT Category, COUNT(*) AS cnt FROM sfpdcrime GROUP BY Category order by cnt desc;
```

<img src="img/Top10Crimes.jpg" width="60%">

```sql
SELECT DayOfWeek, COUNT(*) AS cnt FROM sfpdcrime GROUP BY DayOfWeek order by cnt desc;
```

<img src="img/CrimeByDayOfWeek.jpg" width="50%">

```sql
SELECT category, hour(crimetime) FROM sfpdcrime2;
```

<img src="img/DistributionofCrimebyHour.png" width="60%">


```sql
SELECT pddistrict,category, COUNT(category) FROM sfpdcrime2 GROUP BY pddistrict,category;
```

<img src="img/Top10CrimeDistrib.png" width="60%">

<img src="img/CrimeDistribbydistrict.jpg" width="60%">


```sql
SELECT category, x, y, COUNT(*) AS cnt FROM sfpdcrime2 GROUP BY category, x, y;
```

<img src="img/CrimeMapSort.png" width="60%">

### Drug & Narcotic analysis

To analyze the drug & narcotic crimes using our work on IE, we'll query both tables:
```sql
SELECT * FROM sfpdcrime, sfpddrug WHERE sfpdcrime.descript = sfpddrug.descript AND sfpdcrime.category = 'DRUG/NARCOTIC';
```


<img src="img/drugcat.png" width="70%">

<img src="img/drugclass.png" width="70%">

<img src="img/drugstreet.png" width="70%">

<img src="img/drugstreetclass.png" width="70%">


__author__ = 'kamelnebhi'

import pandas as pd
from sklearn import preprocessing
import numpy as np

#read csv file
train = pd.read_csv("../data/train.csv", parse_dates=[0], infer_datetime_format=True, error_bad_lines=False)

# check if fields are null
print sum(train['Descript'].isnull()) 

#split datetime to date and time columns
temp = pd.DatetimeIndex(train['Dates'])
train['Date'] = temp.date
train['Time'] = temp.time
#delete Dates column
del train['Dates']

#concatenate X and Y in a new column Location
#train['Location'] = train.X

# dummify rank
# dummy_days = pd.get_dummies(train['DayOfWeek'], prefix='day')
# dummy_categories = pd.get_dummies(train['Category'], prefix='category')
# dummy_pddistrict = pd.get_dummies(train['PdDistrict'], prefix='pddistrict')
# dummy_resolution = pd.get_dummies(train['Resolution'], prefix='resolution')
#
# #print dummy_categories.head()
#
# # create a clean data frame for the regression
# cols_to_keep = ['Date', 'Time', 'Descript', 'Address','X','Y']
# data = train[cols_to_keep].join(dummy_days.ix[:, 'day':]).join(dummy_categories.ix[:, 'category':]).join(dummy_pddistrict.ix[:, 'pddistrict':]).join(dummy_resolution.ix[:, 'resolution':])


#create output
cols_to_keep = ['Date', 'Time','DayOfWeek','Category','PdDistrict','Resolution','Descript','Address','X','Y']
output = train[cols_to_keep]
output.to_csv("../data/output-train.csv", sep='\t', encoding='utf-8')

#print output.head()

#summarize the data
print output.describe()

#get a unique list of districts
#category = list(set(train.Resolution))
#print len(category)


